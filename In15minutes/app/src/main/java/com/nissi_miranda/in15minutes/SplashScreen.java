package com.nissi_miranda.in15minutes;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;

public class SplashScreen extends Activity
{
    private boolean done = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        if (savedInstanceState != null)
        {
            done = savedInstanceState.getBoolean("done");
        }

        // Hide the status bar:
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (!done)
        {
            setupHandler();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putBoolean("done", done);
        super.onSaveInstanceState(outState);
    }

    private void setupHandler()
    {
        done = true;

        Handler handler = new Handler();

        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                chooseBetweenActivities();
            }
        }, 8000);
    }

    @SuppressWarnings("SpellCheckingInspection")
    private void chooseBetweenActivities()
    {
        int permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        SharedPreferences.Editor editor = In15minutesApp.preferences.edit();

        if (permission == PackageManager.PERMISSION_GRANTED)
        {
            In15minutesApp app = In15minutesApp.getInstance();
            app.previousScreen = this;

            editor.putString("TopActivity", "MainActivity");
            editor.apply();

            Intent intent = new Intent("com.nissi_miranda.in15minutes.MainActivity");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_from_right, 0);
        }
        else
        {
            editor.putString("TopActivity", "LocationActivity");
            editor.apply();

            Intent intent = new Intent("com.nissi_miranda.in15minutes.LocationActivity");
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }
    }
}
