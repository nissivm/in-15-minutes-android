package com.nissi_miranda.in15minutes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

public abstract class BaseActivity extends Activity
{
    private String TAG = "BaseActivity";
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "onCreate(Bundle savedInstanceState)");

        // Hide the status bar:
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        Log.i(TAG, "onResume()");

        In15minutesApp app = In15minutesApp.getInstance();
        app.stopActivityTransitionTimer();
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (dialog != null)
        {
            dialog.dismiss();
            dialog = null;
        }

        In15minutesApp app = In15minutesApp.getInstance();
        app.startActivityTransitionTimer();
    }

    //----------------------------------------------------------------------------------------//
    // On back pressed
    //----------------------------------------------------------------------------------------//

    @Override
    @SuppressWarnings("all")
    public void onBackPressed()
    {
        String topActivity = In15minutesApp.preferences.getString("TopActivity", "");
        String previousTopActivity = In15minutesApp.preferences.getString("PreviousTopActivity", "");

        if (topActivity.equals("MainActivity"))
        {
            In15minutesApp app = In15minutesApp.getInstance();

            if (app.previousScreen != null)
            {
                app.previousScreen.finish();
            }

            super.onBackPressed();
        }
        else if (topActivity.equals("MapActivity"))
        {
            SharedPreferences.Editor editor = In15minutesApp.preferences.edit();
            editor.putString("TopActivity", "MainActivity");
            editor.apply();

            finish();
            overridePendingTransition(0, R.anim.slide_to_right);
        }
        else // Top is PlaceDetailsActivity
        {
            SharedPreferences.Editor editor = In15minutesApp.preferences.edit();
            editor.putString("TopActivity", previousTopActivity);
            editor.putString("PreviousTopActivity", "");
            editor.apply();

            finish();
            overridePendingTransition(0, R.anim.slide_to_bottom);
        }
    }

    //----------------------------------------------------------------------------------------//
    // Error toast
    //----------------------------------------------------------------------------------------//

    void showErrorToast(String message)
    {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}
