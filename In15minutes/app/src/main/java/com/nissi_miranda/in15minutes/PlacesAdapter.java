package com.nissi_miranda.in15minutes;

import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;

@SuppressWarnings("SpellCheckingInspection")
class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.PlaceTypeCell>
{
    private Context context;

    static class PlaceTypeCell extends RecyclerView.ViewHolder
    {
        ImageView placeImage;
        TextView placeName;

        PlaceTypeCell(View itemView)
        {
            super(itemView);

            placeImage = (ImageView) itemView.findViewById(R.id.placeImage);
            placeName = (TextView) itemView.findViewById(R.id.placeName);
        }
    }

    PlacesAdapter(Context context)
    {
        this.context = context;
    }

    @Override
    public PlacesAdapter.PlaceTypeCell onCreateViewHolder(ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();

        LayoutInflater inflater = LayoutInflater.from(context);
        View placeTypeView = inflater.inflate(R.layout.place_type, parent, false);

        return new PlaceTypeCell(placeTypeView);
    }

    @Override
    public void onBindViewHolder(final PlacesAdapter.PlaceTypeCell cell, int position)
    {
        cell.placeImage.setImageResource(placeImagesArray[position]);
        cell.placeName.setText(placesNamesArray[position]);

        cell.placeImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MainActivity activity = (MainActivity) context;
                int pos = cell.getAdapterPosition();
                activity.initiatePlacesFetchingProcess(placesNamesArray[pos], placeCategoriesArray[pos], placeImagesArray[pos]);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return placeImagesArray.length;
    }

    // List Data:

    private Integer[] placeImagesArray = {
            R.drawable.restaurants, R.drawable.cafes,
            R.drawable.bakeries, R.drawable.bars,
            R.drawable.supermarkets, R.drawable.bookstores,
            R.drawable.shopping_malls, R.drawable.home_goods,
            R.drawable.pet_stores, R.drawable.museums,
            R.drawable.theaters, R.drawable.movie_theaters,
            R.drawable.churches, R.drawable.parks,
            R.drawable.bus_station, R.drawable.subway_station,
            R.drawable.beauty_salon, R.drawable.gym,
            R.drawable.pharmacies, R.drawable.hospitals
    };

    private String[] placesNamesArray = {
            "Restaurants", "Cafes", "Bakeries", "Bars", "Supermarkets", "Bookstores",
            "Shopping", "Home goods", "Pets", "Museums", "Theaters", "Movie theaters",
            "Churches", "Parks, Plazas & Zoos", "Bus stations", "Subway stations",
            "Beauty salons", "Gym", "Pharmacies", "Hospitals"
    };

    private String[] placeCategoriesArray = {
            "foodtrucks,restaurants,kiosk,dancerestaurants",
            "acaibowls,bubbletea,churros,coffee,convenience,cupcakes,delicatessen,empanadas,friterie,gelato,icecream,internetcafe,milkshakebars,cakeshop,pretzels,tea,tortillas,coffeeshops",
            "bakeries,bagels,donuts",
            "barcrawl,bars,beergardens,jazzandblues,pianobars,poolhalls",
            "butcher,ethicgrocery,farmersmarket,fishmonger,grocery,intlgrocery,organic_stores,markets,seafoodmarkets",
            "bookstores,comicbooks,musicvideo,mags,usedbooks",
            "childcloth,deptstores,hats,lingerie,maternity,menscloth,plus_size_fashion,shoes,sleepwear,sportswear,swimwear,vintage,womenscloth,shoppingcenters,thrift_stores,watches,wigs",
            "furniture,homedecor,mattresses,gardening,outdoorfurniture,paintstores,tableware",
            "emergencypethospital,groomer,vet",
            "galleries,museums,planetarium",
            "opera,theater",
            "movietheaters",
            "churches,synagogues",
            "gardens,parks,playgrounds,publicplazas,zoos",
            "busstations",
            "metrostations",
            "barbers,eyebrowservices,hair_extensions,waxing,hair,makeupartists,othersalons",
            "cardioclasses,dancestudio,gyms,pilates,swimminglessons,taichi,yoga,gymnastics",
            "drugstores,herbalshops,pharmacy,vitaminssupplements",
            "emergencyrooms,hospitals,medcenters,urgent_care"
    };
}
