package com.nissi_miranda.in15minutes;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Callback;

@SuppressWarnings("all")
class BusinessInfoWindow implements InfoWindowAdapter
{
    private final View myContentsView;
    private int placeImgId;
    private Resources resources;
    private Context context;

    BusinessInfoWindow(LayoutInflater inflater, int imgId, Resources resources, Context context)
    {
        myContentsView = inflater.inflate(R.layout.business_info_window, null);
        placeImgId = imgId;
        this.resources = resources;
        this.context = context;
    }

    @Override
    public View getInfoContents(Marker marker)
    {
        Business business = (Business) marker.getTag();
        String placeImageUrl = business.getPlaceImageUrl();
        String placeName = business.getPlaceName();
        double rating = business.getPlaceRating();
        int reviewCount = business.getPlaceReviewCount();
        double distanceInSeconds = business.getDistance();
        double distanceInMinutes = distanceInSeconds/60;

        ImageView placeImgView = (ImageView) myContentsView.findViewById(R.id.placeImage);
        AppCompatTextView placeNameTxtView = (AppCompatTextView) myContentsView.findViewById(R.id.placeName);
        ImageView ratingImgView = (ImageView) myContentsView.findViewById(R.id.ratingImage);
        TextView reviewsTxtView = (TextView) myContentsView.findViewById(R.id.reviewCount);
        TextView distanceTxtView = (TextView) myContentsView.findViewById(R.id.distanceText);

        placeNameTxtView.setText(placeName);
        ratingImgView.setImageDrawable(getRatingImage(rating));
        reviewsTxtView.setText(getReviewText(reviewCount));
        distanceTxtView.setText(((int) Math.ceil(distanceInMinutes)) + " min away");

        if (placeImageUrl.equals(""))
        {
            Drawable drawable = resources.getDrawableForDensity(placeImgId, DisplayMetrics.DENSITY_LOW, null);
            placeImgView.setImageDrawable(drawable);
        }
        else
        {
            Picasso.with(context).load(placeImageUrl).placeholder(placeImgId).error(placeImgId)
                    .resize(R.dimen.info_window_image_size, R.dimen.info_window_image_size)
                    .onlyScaleDown().centerCrop().into(placeImgView, new MarkerCallback(marker));
        }

        return myContentsView;
    }

    @Override
    public View getInfoWindow(Marker marker)
    {
        return null;
    }

    private Drawable getRatingImage(double rating)
    {
        Drawable ratingDrawable;
        
        if (rating < 1)
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_0, null);
        }
        else if (rating == 1)
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_1, null);
        }
        else if ((rating > 1) && (rating <= 1.5))
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_1_half, null);
        }
        else if ((rating > 1.5) && (rating <= 2))
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_2, null);
        }
        else if ((rating > 2) && (rating <= 2.5))
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_2_half, null);
        }
        else if ((rating > 2.5) && (rating <= 3))
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_3, null);
        }
        else if ((rating > 3) && (rating <= 3.5))
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_3_half, null);
        }
        else if ((rating > 3.5) && (rating <= 4))
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_4, null);
        }
        else if ((rating > 4) && (rating <= 4.5))
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_4_half, null);
        }
        else
        {
            ratingDrawable = ResourcesCompat.getDrawable(resources, R.drawable.stars_5, null);
        }

        return ratingDrawable;
    }

    private String getReviewText(int reviewCount)
    {
        String reviewText = reviewCount + " Reviews";

        if (reviewCount == 1)
        {
            reviewText = reviewCount + " Review";
        }

        return reviewText;
    }

    static class MarkerCallback implements Callback
    {
        Marker marker = null;

        MarkerCallback(Marker marker)
        {
            this.marker = marker;
        }

        @Override
        public void onError() {}

        @Override
        public void onSuccess()
        {
            if (marker == null)
            {
                return;
            }

            if (!marker.isInfoWindowShown())
            {
                return;
            }

            marker.hideInfoWindow();
            marker.showInfoWindow();
        }
    }
}
