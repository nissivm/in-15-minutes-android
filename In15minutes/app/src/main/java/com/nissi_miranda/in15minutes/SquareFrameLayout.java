package com.nissi_miranda.in15minutes;

import android.widget.FrameLayout;
import android.content.Context;
import android.util.AttributeSet;
import android.os.Build.VERSION_CODES;
import android.annotation.TargetApi;

// A FrameLayout that will always be square - same width and height

public class SquareFrameLayout extends FrameLayout
{
    public SquareFrameLayout(Context context)
    {
        super(context);
    }

    public SquareFrameLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SquareFrameLayout(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(VERSION_CODES.LOLLIPOP)
    public SquareFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int side;

        if (widthMeasureSpec > heightMeasureSpec)
        {
            side = heightMeasureSpec;
        }
        else
        {
            side = widthMeasureSpec;
        }

        // Set a square layout.
        super.onMeasure(side, side);
    }
}
