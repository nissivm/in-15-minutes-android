package com.nissi_miranda.in15minutes;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@SuppressWarnings("SpellCheckingInspection")
public class PlaceDetailsActivity extends BaseActivity implements OnStreetViewPanoramaReadyCallback, OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, ActivityCompat.OnRequestPermissionsResultCallback, ServerRequestInterface
{
    private String TAG = "PlaceDetailsActivity";
    static private final String GOOGLE_MAPS_ANDROID_API_KEY = "AIzaSyC6jNRoAyq71GCTyu-hkThumTlb-YNU684";
    private GoogleMap map;
    private boolean panoramaIsBig = false;
    private boolean routeMapIsShowing = false;
    private boolean routeMapIsBig = false;
    private boolean generatingRoute = false;
    private int phoneCallPermissionRequestCode = 0;

    private String geometry;
    private ArrayList<LatLng> markersCoordinates;
    private ArrayList<String> instructions;

    private ServerRequest serverRequest;

    private FrameLayout containerOne;
    private ImageView enlargeShrinkPanoramaButton;
    private FrameLayout containerTwo;
    private ScrollView placeInfoContainer;
    private FrameLayout routeMapContainer;
    private MapView routeMap;
    private ImageView enlargeShrinkRouteMapButton;
    private RelativeLayout loadingPanel;

    Business business;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_details);

        serverRequest = new ServerRequest(this);

        Bundle mapViewBundle = null;

        if (savedInstanceState != null)
        {
            mapViewBundle = savedInstanceState.getBundle(GOOGLE_MAPS_ANDROID_API_KEY);
            panoramaIsBig = savedInstanceState.getBoolean("panoramaIsBig");
            routeMapIsShowing = savedInstanceState.getBoolean("routeMapIsShowing");
            routeMapIsBig = savedInstanceState.getBoolean("routeMapIsBig");
            generatingRoute = savedInstanceState.getBoolean("generatingRoute");
            phoneCallPermissionRequestCode = savedInstanceState.getInt("phoneCallPermissionRequestCode");
            geometry = savedInstanceState.getString("geometry");
            markersCoordinates = savedInstanceState.getParcelableArrayList("markersCoordinates");
            instructions = savedInstanceState.getStringArrayList("instructions");
            business = savedInstanceState.getParcelable("business");
        }
        else
        {
            business = getIntent().getParcelableExtra("business");
        }

        containerOne = findViewById(R.id.containerOne);
        StreetViewPanoramaView streetViewPanoramaView = findViewById(R.id.streetViewPanorama);
        enlargeShrinkPanoramaButton = findViewById(R.id.enlargeShrinkPanoramaButton);

        containerTwo = findViewById(R.id.containerTwo);
        placeInfoContainer = findViewById(R.id.placeInfoContainer);
        AppCompatTextView placeName = findViewById(R.id.placeName);
        ImageView ratingImage = findViewById(R.id.ratingImage);
        TextView priceRange = findViewById(R.id.priceRange);
        TextView reviewCount = findViewById(R.id.reviewCount);
        ImageView yelpLogo = findViewById(R.id.yelpLogo);
        TextView distanceText = findViewById(R.id.distanceText);
        ImageView routeButton = findViewById(R.id.routeButton);
        TextView placeAddress = findViewById(R.id.placeAddress);
        Button placePhoneButton = findViewById(R.id.placePhoneButton);
        Button placeUrlButton = findViewById(R.id.placeUrlButton);
        routeMapContainer = findViewById(R.id.routeMapContainer);
        routeMap = findViewById(R.id.routeMap);
        enlargeShrinkRouteMapButton = findViewById(R.id.enlargeShrinkRouteMapButton);
        ImageView closeRouteMapButton = findViewById(R.id.closeRouteMapButton);
        loadingPanel = findViewById(R.id.loadingPanel);

        streetViewPanoramaView.onCreate(savedInstanceState);
        streetViewPanoramaView.getStreetViewPanoramaAsync(this);
        routeMap.onCreate(mapViewBundle);
        routeMap.getMapAsync(this);

        placeName.setText(business.getPlaceName());
        ratingImage.setImageDrawable(getRatingImage(business.getPlaceRating()));

        if (!business.getPlacePriceLevel().equals(""))
        {
            priceRange.setText(business.getPlacePriceLevel());
        }
        else
        {
            priceRange.setText("$$$$");
            priceRange.setTextColor(ResourcesCompat.getColor(getResources(), R.color.dark_gray, null));
            priceRange.setAlpha((float)0.2);
        }

        reviewCount.setText(getReviewText(business.getPlaceReviewCount()));

        double distanceInSeconds = business.getDistance();
        double distanceInMinutes = distanceInSeconds/60;
        String distance = ((int) Math.ceil(distanceInMinutes)) + " min away";
        distanceText.setText(distance);

        placeAddress.setText(business.getPlaceAddr());

        if (!business.getPlacePhone().equals(""))
        {
            placePhoneButton.setText(business.getPlacePhone());
        }
        else
        {
            placePhoneButton.setText(R.string.no_phone_number);
            placePhoneButton.setTypeface(placePhoneButton.getTypeface(), Typeface.ITALIC);
            placePhoneButton.setAllCaps(false);
        }

        if (business.getPlaceUrl().equals(""))
        {
            placeUrlButton.setText(R.string.no_place_url);
            placeUrlButton.setTypeface(placeUrlButton.getTypeface(), Typeface.ITALIC);
            placeUrlButton.setAllCaps(false);
        }

        if (routeMapIsShowing)
        {
            placeInfoContainer.setVisibility(View.GONE);
            routeMapContainer.setVisibility(View.VISIBLE);
        }

        if (routeMapIsBig)
        {
            containerOne.setVisibility(View.GONE);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.shrink_button, null);
            enlargeShrinkRouteMapButton.setImageDrawable(drawable);
        }

        if (panoramaIsBig)
        {
            containerTwo.setVisibility(View.GONE);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.shrink_button, null);
            enlargeShrinkPanoramaButton.setImageDrawable(drawable);
        }

        if (!generatingRoute)
        {
            loadingPanel.setVisibility(View.GONE);
        }

        enlargeShrinkPanoramaButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                togglePanorama();
            }
        });

        yelpLogo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.yelp.com"));
                startActivity(intent);
            }
        });

        routeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (map == null)
                {
                    showErrorToast("An error occurred, please try again in a few seconds.");
                    routeMap.getMapAsync(PlaceDetailsActivity.this);
                    return;
                }

                if (markersCoordinates != null)
                {
                    showHideRouteMap();
                    return;
                }

                if (generatingRoute)
                {
                    return;
                }

                generatingRoute = true;
                loadingPanel.setVisibility(View.VISIBLE);
                serverRequest.generateRoute(business.getPlaceLongitude(), business.getPlaceLatitude());
            }
        });

        placePhoneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!business.getPlacePhone().equals(""))
                {
                    if (getResources().getBoolean(R.bool.isTablet))
                    {
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_LONG;

                        Toast toast = Toast.makeText(context, "This device does not perform phone calls", duration);
                        toast.show();

                        return;
                    }

                    int checkPermission = ContextCompat.checkSelfPermission(PlaceDetailsActivity.this, android.Manifest.permission.CALL_PHONE);

                    if (checkPermission == PackageManager.PERMISSION_GRANTED)
                    {
                        makePhoneCall();
                    }
                    else
                    {
                        askPhoneCallPermission();
                    }
                }
            }
        });

        placeUrlButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!business.getPlaceUrl().equals(""))
                {
                    Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(business.getPlaceUrl()));
                    startActivity(intent);
                }
            }
        });

        enlargeShrinkRouteMapButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                toggleRouteMap();
            }
        });

        closeRouteMapButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showHideRouteMap();
            }
        });

        if (phoneCallPermissionRequestCode == 0)
        {
            Random r = new Random();
            int min = 1000;
            int max = 10000;

            phoneCallPermissionRequestCode = r.nextInt((max - min) + 1) + min;
        }
    }

    @Override
    public void onStart()
    {
        routeMap.onStart();
        super.onStart();
    }

    @Override
    protected void onResume()
    {
        routeMap.onResume();
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        routeMap.onPause();
        super.onPause();
    }

    @Override
    public void onStop()
    {
        routeMap.onStop();
        super.onStop();
    }

    @Override
    public void onLowMemory()
    {
        routeMap.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onDestroy()
    {
        routeMap.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        Bundle mapViewBundle = outState.getBundle(GOOGLE_MAPS_ANDROID_API_KEY);

        if (mapViewBundle == null)
        {
            mapViewBundle = new Bundle();
            outState.putBundle(GOOGLE_MAPS_ANDROID_API_KEY, mapViewBundle);
        }

        outState.putBoolean("panoramaIsBig", panoramaIsBig);
        outState.putBoolean("routeMapIsShowing", routeMapIsShowing);
        outState.putBoolean("routeMapIsBig", routeMapIsBig);
        outState.putBoolean("generatingRoute", generatingRoute);
        outState.putInt("phoneCallPermissionRequestCode", phoneCallPermissionRequestCode);
        outState.putString("geometry", geometry);
        outState.putParcelableArrayList("markersCoordinates", markersCoordinates);
        outState.putStringArrayList("instructions", instructions);
        outState.putParcelable("business", business);

        routeMap.onSaveInstanceState(mapViewBundle);
        super.onSaveInstanceState(outState);
    }

    //----------------------------------------------------------------------------------------//
    // ServerRequestInterface
    //----------------------------------------------------------------------------------------//

    @Override
    public void searchPlacesResult(String result, ArrayList<Business> filteredBusinesses){}

    @Override
    public void routeGenerationResult(String result, String geometry, ArrayList<LatLng> markersCoordinates, ArrayList<String> instructions)
    {
        if (result.equals("Success"))
        {
            generatingRoute = false;
            loadingPanel.setVisibility(View.GONE);

            this.geometry = geometry;
            this.markersCoordinates = markersCoordinates;
            this.instructions = instructions;
            showHideRouteMap();
            handleMapContent();
        }
        else
        {
            displayErrorToast(result);
        }
    }

    //----------------------------------------------------------------------------------------//
    // Show/Hide route map
    //----------------------------------------------------------------------------------------//

    private void showHideRouteMap()
    {
        if (routeMapIsShowing)
        {
            routeMapIsShowing = false;

            if (routeMapIsBig)
            {
                toggleRouteMap();
            }

            placeInfoContainer.setVisibility(View.VISIBLE);
            routeMapContainer.setVisibility(View.GONE);
        }
        else
        {
            routeMapIsShowing = true;
            placeInfoContainer.setVisibility(View.GONE);
            routeMapContainer.setVisibility(View.VISIBLE);
        }
    }

    //----------------------------------------------------------------------------------------//
    // Toggle route map
    //----------------------------------------------------------------------------------------//

    private void toggleRouteMap()
    {
        Drawable drawable;

        if (routeMapIsBig)
        {
            routeMapIsBig = false;
            containerOne.setVisibility(View.VISIBLE);
            drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.enlarge_button, null);
        }
        else
        {
            routeMapIsBig = true;
            containerOne.setVisibility(View.GONE);
            drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.shrink_button, null);
        }

        enlargeShrinkRouteMapButton.setImageDrawable(drawable);
    }

    //----------------------------------------------------------------------------------------//
    // Toggle panorama
    //----------------------------------------------------------------------------------------//

    private void togglePanorama()
    {
        Drawable drawable;

        if (panoramaIsBig)
        {
            panoramaIsBig = false;
            containerTwo.setVisibility(View.VISIBLE);
            drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.enlarge_button, null);
        }
        else
        {
            panoramaIsBig = true;
            containerTwo.setVisibility(View.GONE);
            drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.shrink_button, null);
        }

        enlargeShrinkPanoramaButton.setImageDrawable(drawable);
    }

    //----------------------------------------------------------------------------------------//
    // Ask phone call permission
    //----------------------------------------------------------------------------------------//

    private void askPhoneCallPermission()
    {
        String result = In15minutesApp.preferences.getString("AskedPhoneCallPermissionFirstTime", "does not exist");

        if (result.equals("does not exist"))
        {
            // First time app is asking for phone call permission.

            Log.i(TAG, "askPhoneCallPermission() -> First time asking");

            SharedPreferences.Editor editor = In15minutesApp.preferences.edit();
            editor.putString("AskedPhoneCallPermissionFirstTime", "True");
            editor.apply();

            ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.CALL_PHONE},
                    phoneCallPermissionRequestCode);
        }
        else if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CALL_PHONE))
        {
            // User was asked permission before, but denied.
            // Never ask again was not checked.

            Log.i(TAG, "askPhoneCallPermission() -> Permission was asked before, user denied. Never ask again was not checked.");

            ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.CALL_PHONE},
                    phoneCallPermissionRequestCode);
        }
        else
        {
            // Never ask again was checked.

            Log.i(TAG, "askPhoneCallPermission() -> Never ask again was checked");
            takeMeThereCancelDialog();
        }
    }

    //----------------------------------------------------------------------------------------//
    // Take me there / Cancel dialog
    //----------------------------------------------------------------------------------------//

    private void takeMeThereCancelDialog()
    {
        DialogInterface.OnClickListener takeMeThereListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface d, int which)
            {
                dialog = null;
                startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_SETTINGS));
            }
        };

        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface d, int which)
            {
                dialog = null;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enable phone calls");
        builder.setMessage("Just access device's settings:");
        builder.setCancelable(false);
        builder.setPositiveButton("Take me there", takeMeThereListener);
        builder.setNegativeButton("Cancel", cancelListener);

        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    //----------------------------------------------------------------------------------------//
    // ActivityCompat.OnRequestPermissionsResultCallback
    //----------------------------------------------------------------------------------------//

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        // Hide the status bar:
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (requestCode == phoneCallPermissionRequestCode)
        {
            if ((permissions.length == 1) &&
                 permissions[0].equals(android.Manifest.permission.CALL_PHONE) &&
                (grantResults[0] == PackageManager.PERMISSION_GRANTED))
            {
                makePhoneCall();
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Make phone call
    //----------------------------------------------------------------------------------------//

    private void makePhoneCall()
    {
        String phoneNumber = business.getPlacePhone();
        phoneNumber = phoneNumber.replace("-", "");
        phoneNumber = phoneNumber.replace(" ", "");

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));

        int checkPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);

        if (checkPermission == PackageManager.PERMISSION_GRANTED)
        {
            startActivity(callIntent);
        }
    }

    //----------------------------------------------------------------------------------------//
    // OnStreetViewPanoramaReadyCallback
    //----------------------------------------------------------------------------------------//

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama panorama)
    {
        LatLng latLng = new LatLng(business.getPlaceLatitude(), business.getPlaceLongitude());

        StreetViewPanoramaCamera.Builder builder = new StreetViewPanoramaCamera.Builder( panorama.getPanoramaCamera() );
        builder.tilt(0.0f);
        builder.zoom(0.0f);
        builder.bearing(0.0f);

        panorama.animateTo(builder.build(), 0);
        panorama.setPosition(latLng, 300);
        panorama.setStreetNamesEnabled(true);
    }

    //----------------------------------------------------------------------------------------//
    // OnMapReadyCallback
    //----------------------------------------------------------------------------------------//

    @Override
    public void onMapReady(GoogleMap map)
    {
        this.map = map;
        this.map.setOnMarkerClickListener(this);
        this.map.setInfoWindowAdapter(new InstructionInfoWindow(getLayoutInflater()));

        handleMapContent();
    }

    //----------------------------------------------------------------------------------------//
    // Handle map content
    //----------------------------------------------------------------------------------------//

    void handleMapContent()
    {
        if (map == null)
        {
            return;
        }

        if (markersCoordinates == null)
        {
            return;
        }

        map.clear();

        List<LatLng> polylineCoords = PolyUtil.decode(geometry);
        int polylineWidth = (int) getResources().getDimension(R.dimen.polyline_thickness);
        int pathColor = ResourcesCompat.getColor(getResources(), R.color.green, null);

        PolylineOptions options = new PolylineOptions();
        options.addAll(polylineCoords);
        options.width(polylineWidth);
        options.color(pathColor);
        options.geodesic(true);

        map.addPolyline(options);

        int counter = 0;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (LatLng coordinate : markersCoordinates)
        {
            builder.include(coordinate);

            String step = "Step " + (counter + 1);
            String instruction = instructions.get(counter);

            HashMap<String, String> dic = new HashMap<>();
            dic.put("step", step);
            dic.put("instruction", instruction);

            MarkerOptions mo = new MarkerOptions();
            mo.position(coordinate);
            mo.icon(getMarkerIcon("#262b2e"));

            Marker m = map.addMarker(mo);
            m.setTag(dic);

            counter++;
        }

        LatLngBounds bounds = builder.build();
        int mapPadding = (int) getResources().getDimension(R.dimen.map_padding);
        final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, mapPadding);

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback()
        {
            @Override
            public void onMapLoaded()
            {
                map.animateCamera(cu);
            }
        });
    }

    //----------------------------------------------------------------------------------------//
    // Get marker icon with custom color
    //----------------------------------------------------------------------------------------//

    // Font: http://stackoverflow.com/questions/19076124/android-map-marker-color

    private BitmapDescriptor getMarkerIcon(String hexColor)
    {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(hexColor), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    //----------------------------------------------------------------------------------------//
    // GoogleMap.OnMarkerClickListener
    //----------------------------------------------------------------------------------------//

    @Override
    public boolean onMarkerClick(Marker marker)
    {
        int zoom = (int)map.getCameraPosition().zoom;
        double lat = marker.getPosition().latitude;
        double lng = marker.getPosition().longitude;
        LatLng ll = new LatLng(lat + (double)90/Math.pow(2, zoom), lng);

        marker.showInfoWindow();
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        map.animateCamera(cu);

        return true;

        // Obs.: To preserve the default behavior when you click a marker -> return false;
    }

    //----------------------------------------------------------------------------------------//
    // Get rating image
    //----------------------------------------------------------------------------------------//

    private Drawable getRatingImage(double rating)
    {
        Drawable ratingDrawable;

        if (rating < 1)
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_0, null);
        }
        else if (rating == 1)
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_1, null);
        }
        else if ((rating > 1) && (rating <= 1.5))
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_1_half, null);
        }
        else if ((rating > 1.5) && (rating <= 2))
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_2, null);
        }
        else if ((rating > 2) && (rating <= 2.5))
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_2_half, null);
        }
        else if ((rating > 2.5) && (rating <= 3))
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_3, null);
        }
        else if ((rating > 3) && (rating <= 3.5))
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_3_half, null);
        }
        else if ((rating > 3.5) && (rating <= 4))
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_4, null);
        }
        else if ((rating > 4) && (rating <= 4.5))
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_4_half, null);
        }
        else
        {
            ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.stars_5, null);
        }

        return ratingDrawable;
    }

    //----------------------------------------------------------------------------------------//
    // Get review text
    //----------------------------------------------------------------------------------------//

    private String getReviewText(int reviewCount)
    {
        String reviewText = reviewCount + " Reviews";

        if (reviewCount == 1)
        {
            reviewText = reviewCount + " Review";
        }

        return reviewText;
    }

    //----------------------------------------------------------------------------------------//
    // Display error toast
    //----------------------------------------------------------------------------------------//

    private void displayErrorToast(String message)
    {
        generatingRoute = false;
        loadingPanel.setVisibility(View.GONE);
        showErrorToast(message);
    }
}
