package com.nissi_miranda.in15minutes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

@SuppressWarnings("SpellCheckingInspection")
public class MapActivity extends BaseActivity
{
    //private String TAG = "MapActivity";
    private boolean done = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);

        if (savedInstanceState != null)
        {
            done = savedInstanceState.getBoolean("done");
        }

        if (!done)
        {
            setupMapFragment();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putBoolean("done", done);
        super.onSaveInstanceState(outState);
    }

    //----------------------------------------------------------------------------------------//
    // Setup map fragment
    //----------------------------------------------------------------------------------------//

    private void setupMapFragment()
    {
        done = true;

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.isTablet = false;
        mapFragment.showingResults = true;
        mapFragment.placeName = getIntent().getStringExtra("placeName");
        mapFragment.placeImgId = getIntent().getIntExtra("placeImgId", -1);
        mapFragment.filteredBusinesses = getIntent().getParcelableArrayListExtra("filteredBusinesses");
    }

    //----------------------------------------------------------------------------------------//
    // Show place details activity
    //----------------------------------------------------------------------------------------//

    void showPlaceDetailsActivity(Business business)
    {
        SharedPreferences.Editor editor = In15minutesApp.preferences.edit();
        editor.putString("TopActivity", "PlaceDetailsActivity");
        editor.putString("PreviousTopActivity", "MapActivity");
        editor.apply();

        Intent intent = new Intent("com.nissi_miranda.in15minutes.PlaceDetailsActivity");
        intent.putExtra("business", business);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_bottom, 0);
    }
}
