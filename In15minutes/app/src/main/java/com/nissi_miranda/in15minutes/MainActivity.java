package com.nissi_miranda.in15minutes;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

@SuppressWarnings("SpellCheckingInspection")
public class MainActivity extends BaseActivity implements ConnectionCallbacks, OnConnectionFailedListener, ServerRequestInterface
{
    private String TAG = "MainActivity";
    private GoogleApiClient googleApiClient;
    private boolean isLaunching = true;
    private int userLocationRequestCode = 0;
    private boolean userLocationUpdated = false;
    private RelativeLayout loadingPanel;
    private MapFragment mapFragment; // Becomes not null only if app running in a tablet
    private ServerRequest serverRequest;
    private String placeName = "";
    private String placeCategory = "";
    private int placeImgId;
    private HashMap<String, ArrayList<Business>> searchResults = new HashMap<>();

    private boolean runningOnSimulator = false;
    private LocationCallback callback = new LocationCallback();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serverRequest = new ServerRequest(this);

        if (savedInstanceState != null)
        {
            isLaunching = savedInstanceState.getBoolean("isLaunching");
            userLocationRequestCode = savedInstanceState.getInt("userLocationRequestCode");
        }

        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        boolean isPortrait = getResources().getBoolean(R.bool.isPortrait);
        LinearLayoutManager manager;

        if ((!isTablet && isPortrait) || (isTablet && !isPortrait))
        {
            manager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        }
        else
        {
            manager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        }

        PlacesAdapter adapter = new PlacesAdapter(this);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        Fragment f = getFragmentManager().findFragmentById(R.id.mapFragment);

        // Obs.:
        /* When screen rotates, a new layout file is set to Main Activity, this way,
        *  views like mapFragment and loadingPanel have to be set again, to reference
        *  the views belonging to this new layout file. */

        if (f != null) // Two pane layout (tablet)
        {
            mapFragment = (MapFragment)f;
        }

        loadingPanel = findViewById(R.id.loadingPanel);

        if (googleApiClient == null)
        {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        if (userLocationRequestCode == 0)
        {
            Random r = new Random();
            int min = 1000;
            int max = 10000;

            userLocationRequestCode = r.nextInt((max - min) + 1) + min;
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        if (isLaunching)
        {
            if(googleApiClient.isConnected())
            {
                isLaunching = false;
                getUserLocation();
            }
            else
            {
                googleApiClient.connect();
            }
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        serverRequest = new ServerRequest(this);

        In15minutesApp app = In15minutesApp.getInstance();

        if (app.previousScreen != null)
        {
            app.previousScreen.finish();
            app.previousScreen = null;
        }

        if (app.wasInBackground)
        {
            Log.i(TAG, "onResume() -> App was in background");

            app.wasInBackground = false;
            ServerRequest.fetchingPlaces = false;
            loadingPanel.setVisibility(View.GONE);

            int permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

            if (permission == PackageManager.PERMISSION_GRANTED)
            {
                Log.i(TAG, "onResume() -> Permission granted");

                if(googleApiClient.isConnected())
                {
                    getUserLocation();
                }
                else
                {
                    googleApiClient.connect();
                }
            }
            else
            {
                Log.e(TAG, "onResume() -> Permission NOT granted");
            }

            return;
        }

        if (ServerRequest.fetchingPlaces)
        {
            loadingPanel.setVisibility(View.VISIBLE);
        }
        else
        {
            loadingPanel.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStop()
    {
        if (googleApiClient.isConnected())
        {
            googleApiClient.disconnect();
        }

        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putBoolean("isLaunching", isLaunching);
        outState.putInt("userLocationRequestCode", userLocationRequestCode);
        super.onSaveInstanceState(outState);
    }

    //----------------------------------------------------------------------------------------//
    // Get user location
    //----------------------------------------------------------------------------------------//

    private void getUserLocation()
    {
        int permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED)
        {
            userLocationUpdated = false;

            if (ServerRequest.fetchingPlaces)
            {
                takeMeThereCancelDialog();
            }

            return;
        }

        if (runningOnSimulator)
        {
            SharedPreferences.Editor editor = In15minutesApp.preferences.edit();

            // Location in Lago Jacarey/Fortaleza/Ceará:
            //editor.putFloat("UserLat", (float)-3.802796);
            //editor.putFloat("UserLong", (float)-38.490372);

            // Location in Lisbon, Portugal:
            editor.putFloat("UserLat", (float)38.727186);
            editor.putFloat("UserLong", (float)-9.143785);

            // Location near Main street (Vancouver, BC, Canada):
            //editor.putFloat("UserLat", (float)49.248975);
            //editor.putFloat("UserLong", (float)-123.094981);

            editor.apply();

            return;
        }

        final FusedLocationProviderClient locProvClient = LocationServices.getFusedLocationProviderClient(this);

        callback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult)
            {
                if (locationResult.getLastLocation() != null)
                {
                    userLocationUpdated = true;
                    Location location = locationResult.getLastLocation();
                    locProvClient.removeLocationUpdates(callback);

                    SharedPreferences.Editor editor = In15minutesApp.preferences.edit();

                    // PRODUCTION MODE
                    editor.putFloat("UserLat", (float)location.getLatitude());
                    editor.putFloat("UserLong", (float)location.getLongitude());

                    // Location in Lisbon, Portugal:
                    //editor.putFloat("UserLat", (float)38.727186);
                    //editor.putFloat("UserLong", (float)-9.143785);

                    editor.apply();

                    handleLocation();
                }
                else
                {
                    userLocationUpdated = false;
                    locProvClient.removeLocationUpdates(callback);

                    if (!ServerRequest.fetchingPlaces) { return; }

                    double userLat = (double)In15minutesApp.preferences.getFloat("UserLat", 0);

                    if (userLat != 0)
                    {
                        serverRequest.fetchPlaces(placeCategory);
                    }
                    else
                    {
                        Log.e(TAG, "requestLocationUpdates failed.");
                        displayErrorToast("An error occurred, please try again.");
                    }
                }
            }
        };

        In15minutesApp app = In15minutesApp.getInstance();
        locProvClient.requestLocationUpdates(app.locationRequest, callback, Looper.myLooper());
    }

    //----------------------------------------------------------------------------------------//
    // Handle location
    //----------------------------------------------------------------------------------------//

    @SuppressWarnings("ConstantConditions")
    private void handleLocation()
    {
        if (ServerRequest.fetchingPlaces)
        {
            serverRequest.fetchPlaces(placeCategory);
            return;
        }

        if (searchResults.size() > 0)
        {
            searchResults.clear();
        }

        if (mapFragment != null) // Two pane layout (tablet): place user location in map
        {
            if (!mapFragment.showingResults)
            {
                mapFragment.handleMapContent();
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Take me there / Cancel dialog
    //----------------------------------------------------------------------------------------//

    private void takeMeThereCancelDialog()
    {
        DialogInterface.OnClickListener takeMeThereListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface d, int which)
            {
                dialog = null;
                startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_SETTINGS));
            }
        };

        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface d, int which)
            {
                ServerRequest.fetchingPlaces = false;
                loadingPanel.setVisibility(View.GONE);
                dialog = null;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Enable my location tracking");
        builder.setMessage("Just access device's settings:");
        builder.setCancelable(false);
        builder.setPositiveButton("Take me there", takeMeThereListener);
        builder.setNegativeButton("Cancel", cancelListener);

        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    //----------------------------------------------------------------------------------------//
    // Initiate places fetching
    //----------------------------------------------------------------------------------------//

    protected void initiatePlacesFetchingProcess(String placeName, String placeCategory, int placeImgId) // Called in PlacesAdapter, when user taps place image
    {
        if (ServerRequest.fetchingPlaces)
        //if (fetchingPlaces)
        {
            return;
        }

        this.placeName = placeName;
        this.placeCategory = placeCategory;
        this.placeImgId = placeImgId;

        if (searchResults.size() > 0)
        {
            ArrayList<Business> filteredBusinesses = searchResults.get(this.placeName);

            if (filteredBusinesses != null)
            {
                showSearchResult(filteredBusinesses);
                return;
            }
        }

        ServerRequest.fetchingPlaces = true;
        loadingPanel.setVisibility(View.VISIBLE);

        if (userLocationUpdated)
        {
            serverRequest.fetchPlaces(placeCategory);
            return;
        }

        if(googleApiClient.isConnected())
        {
            getUserLocation();
        }
        else
        {
            googleApiClient.connect();
        }
    }

    //----------------------------------------------------------------------------------------//
    // ServerRequestInterface
    //----------------------------------------------------------------------------------------//

    public void searchPlacesResult(String result, ArrayList<Business> filteredBusinesses)
    {
        if (result.equals("Success"))
        {
            ServerRequest.fetchingPlaces = false;
            loadingPanel.setVisibility(View.GONE);

            ArrayList<Business> arr = new ArrayList<>();
            arr.addAll(filteredBusinesses);
            searchResults.put(placeName, arr);

            if (In15minutesApp.getInstance().wasInBackground) // App is in background
            {
                return;
            }

            showSearchResult(filteredBusinesses);
        }
        else
        {
            displayErrorToast(result);
        }
    }

    @Override
    public void routeGenerationResult(String result, String geometry,
                                      ArrayList<LatLng> markersCoordinates,
                                      ArrayList<String> instructions){}

    //----------------------------------------------------------------------------------------//
    // Show search result
    //----------------------------------------------------------------------------------------//

    private void showSearchResult(ArrayList<Business> filteredBusinesses)
    {
        if (mapFragment != null) // Two pane layout (tablet)
        {
            mapFragment.showingResults = true;
            mapFragment.placeName = placeName;
            mapFragment.placeImgId = placeImgId;
            mapFragment.filteredBusinesses = filteredBusinesses;
            mapFragment.handleMapContent();
        }
        else
        {
            SharedPreferences.Editor editor = In15minutesApp.preferences.edit();
            editor.putString("TopActivity", "MapActivity");
            editor.apply();

            Intent intent = new Intent("com.nissi_miranda.in15minutes.MapActivity");
            intent.putExtra("placeName", placeName);
            intent.putExtra("placeImgId", placeImgId);
            intent.putParcelableArrayListExtra("filteredBusinesses", filteredBusinesses);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_from_right, 0);
        }
    }

    //----------------------------------------------------------------------------------------//
    // Show place details activity
    //----------------------------------------------------------------------------------------//

    void showPlaceDetailsActivity(Business business)
    {
        SharedPreferences.Editor editor = In15minutesApp.preferences.edit();
        editor.putString("TopActivity", "PlaceDetailsActivity");
        editor.putString("PreviousTopActivity", "MainActivity");
        editor.apply();

        Intent intent = new Intent("com.nissi_miranda.in15minutes.PlaceDetailsActivity");
        intent.putExtra("business", business);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_bottom, 0);
    }

    //----------------------------------------------------------------------------------------//
    // onActivityResult
    //----------------------------------------------------------------------------------------//

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        In15minutesApp app = In15minutesApp.getInstance();

        if (resultCode == RESULT_OK)
        {
            if (requestCode == app.googleApiClientRequestCode)
            {
                Log.i(TAG, "onActivityResult -> == RESULT_OK (googleApiClientRequestCode)");
                googleApiClient.connect();
            }
            else if (requestCode == userLocationRequestCode)
            {
                Log.i(TAG, "onActivityResult -> == RESULT_OK (userLocationRequestCode)");
                getUserLocation();
            }
        }
        else
        {
            if (requestCode == app.googleApiClientRequestCode)
            {
                Log.e(TAG, "onActivityResult -> != RESULT_OK (googleApiClientRequestCode)");
            }
            else if (requestCode == userLocationRequestCode)
            {
                Log.e(TAG, "onActivityResult -> != RESULT_OK (userLocationRequestCode)");
            }

            double userLat = (double)In15minutesApp.preferences.getFloat("UserLat", 0);

            if (userLat != 0)
            {
                serverRequest.fetchPlaces(placeCategory);
            }
            else
            {
                displayErrorToast("An error occurred, please try again.");
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // ConnectionCallbacks
    //----------------------------------------------------------------------------------------//

    @Override
    public void onConnected(Bundle connectionHint) // Called after a call to connect() (GoogleApiClient) if connection was successfull
    {
        Log.i(TAG, "onConnected(Bundle bundle)");
        isLaunching = false;
        getUserLocation();
    }

    @Override
    public void onConnectionSuspended (int cause) { Log.i(TAG, "onConnectionSuspended(int i)"); }

    //----------------------------------------------------------------------------------------//
    // OnConnectionFailedListener
    //----------------------------------------------------------------------------------------//

    @Override
    public void onConnectionFailed (@NonNull ConnectionResult result) // Called after a call to connect() (GoogleApiClient) if connection failed
    {
        Log.e(TAG, "onConnectionFailed(ConnectionResult connectionResult)");
        isLaunching = false;
        userLocationUpdated = false;

        if (ServerRequest.fetchingPlaces)
        {
            GoogleApiAvailability avail = GoogleApiAvailability.getInstance();
            int errorCode = result.getErrorCode();
            In15minutesApp app = In15minutesApp.getInstance();

            if (avail.isUserResolvableError(errorCode))
            {
                avail.getErrorDialog(this, errorCode, app.googleApiClientRequestCode).show();
            }
            else
            {
                double userLat = (double)In15minutesApp.preferences.getFloat("UserLat", 0);

                if (userLat != 0)
                {
                    serverRequest.fetchPlaces(placeCategory);
                }
                else
                {
                    displayErrorToast("An error occurred, please try again.");
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Display error toast
    //----------------------------------------------------------------------------------------//

    private void displayErrorToast(String message)
    {
        ServerRequest.fetchingPlaces = false;
        loadingPanel.setVisibility(View.GONE);
        showErrorToast(message);
    }
}
