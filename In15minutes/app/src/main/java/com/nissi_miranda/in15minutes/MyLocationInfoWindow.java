package com.nissi_miranda.in15minutes;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

@SuppressWarnings("all")
class MyLocationInfoWindow implements InfoWindowAdapter
{
    private final View myContentsView;

    MyLocationInfoWindow(LayoutInflater inflater)
    {
        myContentsView = inflater.inflate(R.layout.my_location_info_window, null);
    }

    @Override
    public View getInfoContents(Marker marker)
    {
        TextView locAddr = (TextView) myContentsView.findViewById(R.id.myLocationInfoWindowAddress);
        locAddr.setText(R.string.resolving_address);

        if (marker.getSnippet() == null)
        {
            return myContentsView;
        }

        if (!marker.getSnippet().equals(""))
        {
            locAddr.setText(marker.getSnippet());
        }

        return myContentsView;
    }

    @Override
    public View getInfoWindow(Marker marker)
    {
        return null;
    }
}
