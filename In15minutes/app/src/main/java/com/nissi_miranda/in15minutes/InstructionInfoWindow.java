package com.nissi_miranda.in15minutes;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.support.v7.widget.AppCompatTextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;

@SuppressWarnings("all")
class InstructionInfoWindow implements GoogleMap.InfoWindowAdapter
{
    private final View myContentsView;

    InstructionInfoWindow(LayoutInflater inflater)
    {
        myContentsView = inflater.inflate(R.layout.instruction_info_window, null);
    }

    @Override
    public View getInfoContents(Marker marker)
    {
        HashMap<String, String> dic = (HashMap<String, String>) marker.getTag();

        AppCompatTextView title = (AppCompatTextView) myContentsView.findViewById(R.id.instructionTitle);
        title.setText(dic.get("step"));
        TextView instruction = (TextView) myContentsView.findViewById(R.id.instructionText);
        instruction.setText(dic.get("instruction"));

        return myContentsView;
    }

    @Override
    public View getInfoWindow(Marker marker) { return null; }
}
