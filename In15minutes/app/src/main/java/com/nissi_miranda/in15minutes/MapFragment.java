package com.nissi_miranda.in15minutes;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("SpellCheckingInspection")
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapClickListener
{
    static private final String TAG = "MapFragment";
    static private final String GOOGLE_MAPS_ANDROID_API_KEY = "Your key";

    private LayoutInflater inflater;
    private MapView mapView;
    private FrameLayout infoContainer;
    private TextView placeNameTxtView;
    private Button clearBtn;
    private GoogleMap map;
    private Marker userMarker;
    private boolean alreadyShowedToast = false;
    private AlertDialog dialog;
    private boolean rotated = false;
    private String currentOrientation;
    private String selMarkerId = "";

    boolean isTablet = true;
    boolean showingResults = false;
    String placeName = "";
    int placeImgId = 0;
    ArrayList<Business> filteredBusinesses = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        this.inflater = inflater;

        // Inflate the layout for this fragment:
        View mainView = this.inflater.inflate(R.layout.map_fragment, container, false);

        Bundle mapViewBundle = null;

        if (savedInstanceState != null)
        {
            mapViewBundle = savedInstanceState.getBundle(GOOGLE_MAPS_ANDROID_API_KEY);

            isTablet = savedInstanceState.getBoolean("isTablet");
            showingResults = savedInstanceState.getBoolean("showingResults");
            currentOrientation = savedInstanceState.getString("currentOrientation");
            placeName = savedInstanceState.getString("placeName");
            placeImgId = savedInstanceState.getInt("placeImgId");
            filteredBusinesses = savedInstanceState.getParcelableArrayList("filteredBusinesses");
            alreadyShowedToast = savedInstanceState.getBoolean("alreadyShowedToast");
            selMarkerId = savedInstanceState.getString("selMarkerId");
        }

        boolean isPortrait = getResources().getBoolean(R.bool.isPortrait);

        if (currentOrientation != null)
        {
            if ((isPortrait && currentOrientation.equals("Landscape")) || (!isPortrait && currentOrientation.equals("Portrait")))
            {
                rotated = true;
            }
        }

        if (isPortrait)
        {
            currentOrientation = "Portrait";
        }
        else
        {
            currentOrientation = "Landscape";
        }

        clearBtn = (Button) mainView.findViewById(R.id.clearBtn);

        clearBtn.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                clearBtnTapped();
            }
        });

        infoContainer = (FrameLayout) mainView.findViewById(R.id.infoContainer);
        infoContainer.setVisibility(View.GONE);

        mapView = (MapView) mainView.findViewById(R.id.map);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
        placeNameTxtView = (TextView) mainView.findViewById(R.id.placeName);

        return mainView;
    }

    @Override
    public void onStart()
    {
        mapView.onStart();
        super.onStart();
    }

    @Override
    public void onResume()
    {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause()
    {
        if (dialog != null)
        {
            dialog.dismiss();
            dialog = null;
        }

        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onStop()
    {
        mapView.onStop();
        super.onStop();
    }

    @Override
    public void onLowMemory()
    {
        mapView.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onDestroy()
    {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        Bundle mapViewBundle = outState.getBundle(GOOGLE_MAPS_ANDROID_API_KEY);

        if (mapViewBundle == null)
        {
            mapViewBundle = new Bundle();
            outState.putBundle(GOOGLE_MAPS_ANDROID_API_KEY, mapViewBundle);
        }

        outState.putBoolean("isTablet", isTablet);
        outState.putBoolean("showingResults", showingResults);
        outState.putString("currentOrientation", currentOrientation);
        outState.putString("placeName", placeName);
        outState.putInt("placeImgId", placeImgId);
        outState.putParcelableArrayList("filteredBusinesses", filteredBusinesses);
        outState.putBoolean("alreadyShowedToast", alreadyShowedToast);
        outState.putString("selMarkerId", selMarkerId);

        mapView.onSaveInstanceState(mapViewBundle);
        super.onSaveInstanceState(outState);
    }

    //----------------------------------------------------------------------------------------//
    // Clear button action
    //----------------------------------------------------------------------------------------//

    private void clearBtnTapped()
    {
        DialogInterface.OnClickListener clearListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface d, int which)
            {
                dialog = null;
                infoContainer.setVisibility(View.GONE);
                showingResults = false;
                handleMapContent();
            }
        };

        DialogInterface.OnClickListener neverMindListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface d, int which)
            {
                dialog = null;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Clear search");
        builder.setMessage("Clear current search?");
        builder.setCancelable(false);
        builder.setPositiveButton("Clear", clearListener);
        builder.setNegativeButton("Never mind", neverMindListener);

        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    //----------------------------------------------------------------------------------------//
    // GoogleMap.OnMarkerClickListener
    //----------------------------------------------------------------------------------------//

    @Override
    public boolean onMarkerClick(Marker marker)
    {
        if (marker.getTag() != null)
        {
            map.setInfoWindowAdapter(new BusinessInfoWindow(inflater, placeImgId, getResources(), getActivity()));
            Business business = (Business) marker.getTag();
            selMarkerId = business.getPlaceId();

            if (!alreadyShowedToast)
            {
                alreadyShowedToast = true;

                String msg = "Tap info bubble for more";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(getActivity(), msg, duration);
                toast.show();
            }
        }
        else
        {
            map.setInfoWindowAdapter(new MyLocationInfoWindow(inflater));
            selMarkerId = "";
        }

        int zoom = (int)map.getCameraPosition().zoom;
        double lat = marker.getPosition().latitude;
        double lng = marker.getPosition().longitude;
        LatLng ll = new LatLng(lat + (double)90/Math.pow(2, zoom), lng);

        marker.showInfoWindow();
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        map.animateCamera(cu);

        return true;

        // Obs.: To preserve the default behavior when you click a marker -> return false;
    }

    //----------------------------------------------------------------------------------------//
    // GoogleMap.OnInfoWindowClickListener
    //----------------------------------------------------------------------------------------//

    @Override
    public void onInfoWindowClick(Marker marker)
    {
        if (marker.getTag() != null)
        {
            Business business = (Business) marker.getTag();

            if (MainActivity.class.isInstance(getActivity()))
            {
                MainActivity mc = (MainActivity)getActivity();
                mc.showPlaceDetailsActivity(business);
            }
            else
            {
                MapActivity ma = (MapActivity)getActivity();
                ma.showPlaceDetailsActivity(business);
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // GoogleMap.OnMapClickListener
    //----------------------------------------------------------------------------------------//

    @Override
    public void onMapClick(LatLng latLng) { selMarkerId = ""; }

    //----------------------------------------------------------------------------------------//
    // On map ready
    //----------------------------------------------------------------------------------------//

    @Override
    public void onMapReady(GoogleMap map)
    {
        this.map = map;
        this.map.setOnMarkerClickListener(this);
        this.map.setOnInfoWindowClickListener(this);
        this.map.setOnMapClickListener(this);

        Log.i(TAG, "Map is ready!");

        if (rotated || !isTablet)
        {
            rotated = false;
            handleMapContent();
        }
    }

    //----------------------------------------------------------------------------------------//
    // Handle map content
    //----------------------------------------------------------------------------------------//

    void handleMapContent()
    {
        if (map == null)
        {
            return;
        }

        map.clear();

        if (selMarkerId.equals(""))
        {
            map.setInfoWindowAdapter(new MyLocationInfoWindow(inflater));
        }
        else
        {
            map.setInfoWindowAdapter(new BusinessInfoWindow(inflater, placeImgId, getResources(), getActivity()));
        }

        userMarker = null;

        double userLat = (double)In15minutesApp.preferences.getFloat("UserLat", 0);
        double userLong = (double)In15minutesApp.preferences.getFloat("UserLong", 0);

        LatLng latLng = new LatLng(userLat, userLong);
        String green = "#03c174";

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(getMarkerIcon(green));

        userMarker = map.addMarker(markerOptions);
        getLastUserAddress();
        dropMarkerAnimation();

        int zoom = 19;
        map.setBuildingsEnabled(true);
        map.setTrafficEnabled(true);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setMapToolbarEnabled(false);

        if (showingResults)
        {
            zoom = 16;
            infoContainer.setVisibility(View.VISIBLE);

            if (!isTablet)
            {
                clearBtn.setVisibility(View.GONE);
            }

            placeNameTxtView.setText(placeName);

            for (Business business : filteredBusinesses)
            {
                double pLat = business.getPlaceLatitude();
                double pLong = business.getPlaceLongitude();
                LatLng pLatLng = new LatLng(pLat, pLong);

                MarkerOptions mo = new MarkerOptions();
                mo.position(pLatLng);
                mo.icon(getMarkerIcon("#262b2e"));

                Marker m = map.addMarker(mo);
                m.setTag(business);

                if (selMarkerId.equals(business.getPlaceId()))
                {
                    latLng = pLatLng;
                    m.showInfoWindow();
                }
            }
        }

        LatLng ll = new LatLng(latLng.latitude + (double)90/Math.pow(2, zoom), latLng.longitude);
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        map.animateCamera(cu);
    }

    //----------------------------------------------------------------------------------------//
    // Get last user address
    //----------------------------------------------------------------------------------------//

    private void getLastUserAddress()
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                final List<Address> addresses;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try
                {
                    double userLat = (double)In15minutesApp.preferences.getFloat("UserLat", 0);
                    double userLong = (double)In15minutesApp.preferences.getFloat("UserLong", 0);
                    addresses = geocoder.getFromLocation(userLat, userLong, 1);

                    getActivity().runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            if (addresses == null)
                            {
                                Log.i(TAG, "addresses == null");
                                return;
                            }

                            if (addresses.size() == 0)
                            {
                                Log.i(TAG, "addresses.size() == 0");
                                return;
                            }

                            if (userMarker == null)
                            {
                                Log.i(TAG, "userMarker == null");
                                return;
                            }

                            Log.i(TAG, "Address found (1): " + addresses.get(0));

                            String addr = addresses.get(0).getAddressLine(0);
                            Log.i(TAG, "Address found (2): " + addr);

                            userMarker.setSnippet(addr);

                            if (userMarker.isInfoWindowShown())
                            {
                                userMarker.hideInfoWindow();
                                userMarker.showInfoWindow();
                            }
                        }
                    });
                }
                catch (final IOException ioException) // Catch network or other I/O problems.
                {
                    getActivity().runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Log.e(TAG, "IOException while trying to obtain address from coordinates => " + ioException);
                        }
                    });
                }
                catch (final IllegalArgumentException illegalArgumentException) // Catch invalid latitude or longitude values.
                {
                    getActivity().runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Log.e(TAG, "IllegalArgumentException while trying to obtain address from coordinates => " + illegalArgumentException);
                        }
                    });
                }
            }
        }).start();
    }

    //----------------------------------------------------------------------------------------//
    // Drop marker animation
    //----------------------------------------------------------------------------------------//

    // Font: http://stackoverflow.com/questions/32182297/android-mapbox-how-can-i-animate-markers-like-marker-falling-from-the-top-of-s

    private void dropMarkerAnimation()
    {
        // Handler allows us to repeat a code block after a specified delay
        final android.os.Handler handler = new android.os.Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1500;

        // Use the bounce interpolator
        final android.view.animation.Interpolator interpolator = new BounceInterpolator();

        // Animate marker with a bounce updating its position every 15ms
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                long elapsed = SystemClock.uptimeMillis() - start;

                // Calculate t for bounce based on elapsed time
                float t = Math.max(1 - interpolator.getInterpolation((float) elapsed / duration), 0);

                // Set the anchor
                userMarker.setAnchor(0.5f, 1.0f + 14 * t);

                if (t > 0.0)
                {
                    // Post this event again 15ms from now.
                    handler.postDelayed(this, 15);
                }
                else
                {
                    if (selMarkerId.equals(""))
                    {
                        // done elapsing, show window
                        userMarker.showInfoWindow();
                    }
                }
            }
        });
    }

    //----------------------------------------------------------------------------------------//
    // Get marker icon with custom color
    //----------------------------------------------------------------------------------------//

    // Font: http://stackoverflow.com/questions/19076124/android-map-marker-color

    private BitmapDescriptor getMarkerIcon(String hexColor)
    {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(hexColor), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}