package com.nissi_miranda.in15minutes;

import android.app.AlertDialog;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.LocationSettingsResponse;

import java.util.Random;

public class LocationActivity extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
    private String TAG = "LocationActivity";
    private GoogleApiClient googleApiClient;
    private int locationPermissionRequestCode = 0;
    private int locationSettingsRequestCode = 0;
    private boolean isReturnedFromSettings = false;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_activity);

        if (savedInstanceState != null)
        {
            locationPermissionRequestCode = savedInstanceState.getInt("locationPermissionRequestCode");
            locationSettingsRequestCode = savedInstanceState.getInt("locationSettingsRequestCode");
        }

        // Hide the status bar:
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        AppCompatButton button = findViewById(R.id.allowLocationBtn);

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int permission = ContextCompat.checkSelfPermission(LocationActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);

                if (permission != PackageManager.PERMISSION_GRANTED)
                {
                    Log.i(TAG, "setOnClickListener -> onClick -> askLocationPermission()");
                    askLocationPermission();
                }
                else
                {
                    Log.i(TAG, "setOnClickListener -> onClick -> checkGoogleApiClient()");
                    checkGoogleApiClient();
                }
            }
        });

        if (googleApiClient == null)
        {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        if (locationPermissionRequestCode == 0)
        {
            Random r = new Random();
            int min = 1000;
            int max = 10000;

            locationPermissionRequestCode = r.nextInt((max - min) + 1) + min;
            locationSettingsRequestCode = r.nextInt((max - min) + 1) + min;
        }
    }

    @Override
    protected void onResume()
    {
        if (isReturnedFromSettings)
        {
            isReturnedFromSettings = false;

            Log.i(TAG, "onResume() -> isReturnedFromSettings");

            int permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

            if (permission == PackageManager.PERMISSION_GRANTED)
            {
                Log.i(TAG, "onResume() -> isReturnedFromSettings: PERMISSION_GRANTED");
                checkGoogleApiClient();
            }
        }

        super.onResume();
    }

    @Override
    protected void onStop()
    {
        if (googleApiClient.isConnected())
        {
            googleApiClient.disconnect();
        }

        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("locationPermissionRequestCode", locationPermissionRequestCode);
        outState.putInt("locationSettingsRequestCode", locationSettingsRequestCode);
        super.onSaveInstanceState(outState);
    }

    //----------------------------------------------------------------------------------------//
    // Ask location permission
    //----------------------------------------------------------------------------------------//

    private void askLocationPermission()
    {
        String result = In15minutesApp.preferences.getString("AskedLocationPermissionFirstTime", "does not exist");

        if (result.equals("does not exist"))
        {
            // First time app is asking for location permission.

            Log.i(TAG, "askLocationPermission() -> First time asking");

            SharedPreferences.Editor editor = In15minutesApp.preferences.edit();
            editor.putString("AskedLocationPermissionFirstTime", "True");
            editor.apply();

            ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION},
                    locationPermissionRequestCode);
        }
        else if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION))
        {
            // User was asked permission before, but denied.
            // Never ask again was not checked.

            Log.i(TAG, "askLocationPermission() -> Permission was asked before, user denied. Never ask again was not checked.");

            ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION},
                    locationPermissionRequestCode);
        }
        else
        {
            // Never ask again was checked.

            Log.i(TAG, "askLocationPermission() -> Never ask again was checked");
            takeMeThereCancelDialog();
        }
    }

    //----------------------------------------------------------------------------------------//
    // Take me there / Cancel dialog
    //----------------------------------------------------------------------------------------//

    private void takeMeThereCancelDialog()
    {
        DialogInterface.OnClickListener takeMeThereListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface d, int which)
            {
                isReturnedFromSettings = true;
                dialog = null;
                startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_SETTINGS));
            }
        };

        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface d, int which)
            {
                dialog = null;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enable my location tracking");
        builder.setMessage("Just access device's settings:");
        builder.setCancelable(false);
        builder.setPositiveButton("Take me there", takeMeThereListener);
        builder.setNegativeButton("Cancel", cancelListener);

        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    //----------------------------------------------------------------------------------------//
    // ActivityCompat.OnRequestPermissionsResultCallback
    //----------------------------------------------------------------------------------------//

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        // Hide the status bar:
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (requestCode == locationPermissionRequestCode)
        {
            if ((permissions.length == 1) &&
                 permissions[0].equals(android.Manifest.permission.ACCESS_FINE_LOCATION) &&
                (grantResults[0] == PackageManager.PERMISSION_GRANTED))
            {
                Log.i(TAG, "onRequestPermissionsResult -> PERMISSION_GRANTED");
                checkGoogleApiClient();
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Check Google Api Client
    //----------------------------------------------------------------------------------------//

    private void checkGoogleApiClient()
    {
        if(googleApiClient.isConnected())
        {
            checkLocationSettings();
        }
        else
        {
            googleApiClient.connect();
        }
    }

    //----------------------------------------------------------------------------------------//
    // Check location settings
    //----------------------------------------------------------------------------------------//

    @SuppressWarnings("SpellCheckingInspection")
    private void checkLocationSettings()
    {
        In15minutesApp app = In15minutesApp.getInstance();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(app.locationRequest);

        Task<LocationSettingsResponse> task =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());

        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {

            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {

                try
                {
                    task.getResult(ApiException.class);

                    // All location settings are satisfied.
                    // The client can initialize location requests here.

                    Log.i(TAG, "Location settings satisfied");

                    In15minutesApp app = In15minutesApp.getInstance();
                    app.previousScreen = LocationActivity.this;

                    SharedPreferences.Editor editor = In15minutesApp.preferences.edit();
                    editor.putString("TopActivity", "MainActivity");
                    editor.apply();

                    Intent intent = new Intent("com.nissi_miranda.in15minutes.MainActivity");
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_from_right, 0);
                }
                catch (ApiException exception)
                {
                    switch (exception.getStatusCode())
                    {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            // Location settings are not satisfied, but this can be fixed
                            // by showing the user a dialog.

                            Log.i(TAG, "LocationSettingsStatusCodes.RESOLUTION_REQUIRED");

                            try
                            {
                                ResolvableApiException resolvable = (ResolvableApiException) exception;

                                // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult():
                                resolvable.startResolutionForResult(LocationActivity.this, locationSettingsRequestCode);
                            }
                            catch (IntentSender.SendIntentException e)
                            {
                                // Resolution intent has been canceled or is no longer able to execute the request.

                                Log.e(TAG, "Failure while starting resolution for result");
                                showErrorToast();
                            }

                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            // Location settings are not satisfied. However, we have no way
                            // to fix the settings so we won't show the dialog.

                            Log.i(TAG, "LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE");

                            String msg = "Your device was not able to meet the necessary requirements to retrieve your location in a satisfactory manner.";

                            DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface d, int which)
                                {
                                    dialog = null;
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(LocationActivity.this);
                            builder.setMessage(msg);
                            builder.setCancelable(false);
                            builder.setPositiveButton("Ok", okListener);

                            dialog = builder.create();
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();

                            break;
                    }
                }
            }
        });
    }

    //----------------------------------------------------------------------------------------//
    // onActivityResult
    //----------------------------------------------------------------------------------------//

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // Hide the status bar:
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        In15minutesApp app = In15minutesApp.getInstance();

        if (resultCode == RESULT_OK)
        {
            if (requestCode == locationSettingsRequestCode)
            {
                Log.i(TAG, "onActivityResult -> == RESULT_OK (locationSettingsRequestCode)");
                checkLocationSettings();
            }
            else if (requestCode == app.googleApiClientRequestCode)
            {
                Log.i(TAG, "onActivityResult -> == RESULT_OK (googleApiClientRequestCode)");
                googleApiClient.connect();
            }
        }
        else
        {
            if (requestCode == locationSettingsRequestCode)
            {
                Log.e(TAG, "onActivityResult -> != RESULT_OK (locationSettingsRequestCode)");
            }
            else if (requestCode == app.googleApiClientRequestCode)
            {
                Log.e(TAG, "onActivityResult -> != RESULT_OK (googleApiClientRequestCode)");
            }

            showErrorToast();
        }
    }

    //----------------------------------------------------------------------------------------//
    // ConnectionCallbacks
    //----------------------------------------------------------------------------------------//

    @Override
    public void onConnected(@Nullable Bundle bundle)
    {
        Log.i(TAG, "onConnected(Bundle bundle)");
        checkLocationSettings();
    }

    @Override
    public void onConnectionSuspended(int i) { Log.i(TAG, "onConnectionSuspended(int i)"); }

    //----------------------------------------------------------------------------------------//
    // OnConnectionFailedListener
    //----------------------------------------------------------------------------------------//

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result)
    {
        Log.e(TAG, "onConnectionFailed(ConnectionResult connectionResult)");

        GoogleApiAvailability avail = GoogleApiAvailability.getInstance();
        int errorCode = result.getErrorCode();

        if (avail.isUserResolvableError(errorCode))
        {
            In15minutesApp app = In15minutesApp.getInstance();
            avail.getErrorDialog(this, errorCode, app.googleApiClientRequestCode).show();
        }
        else
        {
            showErrorToast();
        }
    }

    //----------------------------------------------------------------------------------------//
    // Error toast
    //----------------------------------------------------------------------------------------//

    void showErrorToast()
    {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;
        String message = "An error occurred, please try again.";

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}
