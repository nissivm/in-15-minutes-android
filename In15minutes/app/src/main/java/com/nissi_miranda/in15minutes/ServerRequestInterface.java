package com.nissi_miranda.in15minutes;

import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;

interface ServerRequestInterface
{
    void searchPlacesResult(String result, ArrayList<Business> filteredBusinesses);
    void routeGenerationResult(String result, String geometry, ArrayList<LatLng> markersCoordinates, ArrayList<String> instructions);
}
