package com.nissi_miranda.in15minutes;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressWarnings("SpellCheckingInspection")
class ServerRequest
{
    static private final String MAPBOX_TOKEN = "Your token";

    static boolean fetchingPlaces = false;

    private ServerRequestInterface interf;
    private final String TAG = "ServerRequest";
    private int index = 0;
    private ArrayList<Business> businesses = new ArrayList<>();
    private ArrayList<Business> filteredBusinesses = new ArrayList<>();

    ServerRequest(ServerRequestInterface i) { interf = i; }

    //----------------------------------------------------------------------------------------//
    // Fetch places
    //----------------------------------------------------------------------------------------//

    void fetchPlaces(String placeCategory)
    {
        double userLat = (double)In15minutesApp.preferences.getFloat("UserLat", 0);
        double userLong = (double)In15minutesApp.preferences.getFloat("UserLong", 0);

        String url = "https://api.yelp.com/v3/businesses/search?" +
                     "latitude=" + userLat + "&longitude=" + userLong +
                     "&categories=" + placeCategory + "&locale=en_US" +
                     "&limit=" + 50 + "&sort_by=distance";

        PlacesRequest placesRequest = new PlacesRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            JSONArray businessesArray = response.getJSONArray("businesses");

                            if (businessesArray.length() > 0)
                            {
                                Log.i(TAG, "Returned businesses: " + businessesArray);
                                Log.i(TAG, "Returned businesses amount: " + businessesArray.length());

                                if (businesses.size() > 0)
                                {
                                    businesses.clear();
                                }

                                if (filteredBusinesses.size() > 0)
                                {
                                    filteredBusinesses.clear();
                                }

                                createBusinessObjects(businessesArray);
                            }
                            else
                            {
                                Log.i(TAG, "Search returned no businesses");
                                interf.searchPlacesResult("Search returned no results for your surroundings.", null);
                            }
                        }
                        catch (JSONException e)
                        {
                            Log.e(TAG, "Error while getting businesses array inside json:");
                            e.printStackTrace();
                            interf.searchPlacesResult("An error occurred, please try again.", null);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e(TAG, "Error while fetching businesses: " + error.toString());
                        interf.searchPlacesResult("An error occurred or this app has reached it's daily search quota.", null);
                    }
                });

        In15minutesApp app = In15minutesApp.getInstance();
        app.addJsonRequestToQueue(placesRequest);
    }

    //----------------------------------------------------------------------------------------//
    // Create Business objects
    //----------------------------------------------------------------------------------------//

    private void createBusinessObjects(JSONArray businessesArray)
    {
        try
        {
            JSONObject obj = businessesArray.getJSONObject(index);

            Business business = new Business(obj);

            if (!business.isPlaceClosed())
            {
                if (!business.getPlaceName().equals("") && !business.getPlaceAddr().equals("") &&
                    (business.getPlaceLatitude() != 0) && (business.getPlaceLongitude() != 0))
                {
                    businesses.add(business);
                }
            }

            if (index < (businessesArray.length() - 1))
            {
                index++;
                createBusinessObjects(businessesArray);
            }
            else
            {
                Log.i(TAG, "Created businesses amount: " + businesses.size());

                index = 0;

                if (businesses.size() > 0)
                {
                    fetchDistances();
                }
                else
                {
                    interf.searchPlacesResult("Search returned no results for your surroundings.", null);
                }
            }
        }
        catch (JSONException e)
        {
            Log.e(TAG, "Error while extracting business dic from JSONArray:");
            e.printStackTrace();

            if (index < (businessesArray.length() - 1))
            {
                index++;
                createBusinessObjects(businessesArray);
            }
            else
            {
                index = 0;

                if (businesses.size() > 0)
                {
                    fetchDistances();
                }
                else
                {
                    interf.searchPlacesResult("Search returned no results for your surroundings.", null);
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Fetch distances
    //----------------------------------------------------------------------------------------//

    @SuppressWarnings("all")
    private void fetchDistances()
    {
        double userLong = (double)In15minutesApp.preferences.getFloat("UserLong", 0);
        double userLat = (double)In15minutesApp.preferences.getFloat("UserLat", 0);

        final double maxDuration = 900; // Equivalent in seconds of 15 minutes
        String coordinates = userLong + "," + userLat + ";";
        String destinations = "destinations=";
        int counter = 0;

        for (Business business : businesses)
        {
            double placeLong = business.getPlaceLongitude();
            double placeLat = business.getPlaceLatitude();

            coordinates += placeLong + "," + placeLat;
            destinations += (counter + 1);

            if ((counter < (businesses.size() - 1)) && (counter < 23))
            {
                coordinates += ";";
                destinations += ";";
            }

            if (counter < 23)
            {
                counter++;
            }
            else
            {
                break;
            }
        }

        String url = "https://api.mapbox.com/directions-matrix/v1/mapbox/walking/" + coordinates +
                "?sources=0&" + destinations + "&access_token=" + MAPBOX_TOKEN;

        Log.i(TAG, "Distances url => " + url);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Log.i(TAG, "Fetch distances server response => " + response);

                        try
                        {
                            JSONArray arr = response.getJSONArray("durations");
                            JSONArray durationsArr = (JSONArray)arr.get(0);

                            Log.i(TAG, "durationsArr => " + durationsArr);

                            if (durationsArr.length() == 0)
                            {
                                if (filteredBusinesses.size() > 0)
                                {
                                    interf.searchPlacesResult("Success", filteredBusinesses);
                                }
                                else
                                {
                                    interf.searchPlacesResult("An error occurred, please try again.", null);
                                }

                                return;
                            }

                            for (int i = 0; i < durationsArr.length(); i++)
                            {
                                if (!durationsArr.get(i).equals(null))
                                {
                                    double duration = durationsArr.getDouble(i);

                                    if (duration <= maxDuration)
                                    {
                                        Business business = businesses.get(0);
                                        business.setDistance(duration);
                                        filteredBusinesses.add(business);
                                    }
                                }

                                businesses.remove(0);
                            }

                            if (businesses.size() > 0)
                            {
                                fetchDistances();
                            }
                            else if (filteredBusinesses.size() > 0)
                            {
                                interf.searchPlacesResult("Success", filteredBusinesses);
                            }
                            else
                            {
                                interf.searchPlacesResult("Search returned no results for your surroundings.", null);
                            }
                        }
                        catch (JSONException e)
                        {
                            Log.e(TAG, "Error while fetching distances:");
                            e.printStackTrace();

                            if (filteredBusinesses.size() > 0)
                            {
                                interf.searchPlacesResult("Success", filteredBusinesses);
                            }
                            else
                            {
                                interf.searchPlacesResult("An error occurred, please try again.", null);
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e(TAG, "Error while fetching distances: " + error.toString());

                        if (filteredBusinesses.size() > 0)
                        {
                            interf.searchPlacesResult("Success", filteredBusinesses);
                        }
                        else
                        {
                            interf.searchPlacesResult("An error occurred, please try again.", null);
                        }
                    }
                });

        In15minutesApp app = In15minutesApp.getInstance();
        app.addJsonRequestToQueue(jsObjRequest);
    }

    //----------------------------------------------------------------------------------------//
    // Generate route
    //----------------------------------------------------------------------------------------//

    void generateRoute(double destLng, double destLat)
    {
        double originLng = (double)In15minutesApp.preferences.getFloat("UserLong", 0);
        double originLat = (double)In15minutesApp.preferences.getFloat("UserLat", 0);

        String url = "https://api.mapbox.com/directions/v5/mapbox/walking/" + originLng + "," + originLat + ";" +
                     destLng + "," + destLat + "?steps=true&access_token=" + MAPBOX_TOKEN;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Log.i(TAG, "Generate route server response => " + response);

                        if (interf == null)
                        {
                            return;
                        }

                        try
                        {
                            JSONArray routes = response.getJSONArray("routes");
                            JSONObject routesFirstItem = (JSONObject) routes.get(0);
                            String geometry = routesFirstItem.getString("geometry");

                            JSONArray legs = routesFirstItem.getJSONArray("legs");
                            JSONObject legsFirstItem = (JSONObject) legs.get(0);
                            JSONArray steps = legsFirstItem.getJSONArray("steps");

                            ArrayList<LatLng> markersCoordinates = new ArrayList<>();
                            ArrayList<String> instructions = new ArrayList<>();

                            for (int i = 0; i < steps.length(); i++)
                            {
                                JSONObject step = steps.getJSONObject(i);
                                JSONObject maneuver = step.getJSONObject("maneuver");

                                JSONArray location = maneuver.getJSONArray("location");
                                double lng = location.getDouble(0);
                                double lat = location.getDouble(1);
                                LatLng latLng = new LatLng(lat, lng);
                                markersCoordinates.add(latLng);

                                String instruction = maneuver.getString("instruction");
                                instructions.add(instruction);
                            }

                            interf.routeGenerationResult("Success", geometry, markersCoordinates, instructions);
                        }
                        catch (JSONException e)
                        {
                            Log.e(TAG, "Error while generating route:");
                            e.printStackTrace();
                            interf.routeGenerationResult("An error occurred, please try again.", null, null, null);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        if (interf == null)
                        {
                            return;
                        }

                        Log.e(TAG, "Error while generating route: " + error.toString());
                        interf.routeGenerationResult("An error occurred, please try again.", null, null, null);
                    }
                });

        In15minutesApp app = In15minutesApp.getInstance();
        app.addJsonRequestToQueue(jsObjRequest);
    }
}
