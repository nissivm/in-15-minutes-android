package com.nissi_miranda.in15minutes;

import android.app.Application;
import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.RequestQueue;
import com.google.android.gms.location.LocationRequest;

@SuppressWarnings("SpellCheckingInspection")
public class In15minutesApp extends Application
{
    private static final String TAG = "In15minutesApp";
    private static final String PREFS_NAME = "In15Minutes";

    Activity previousScreen;
    LocationRequest locationRequest;
    int googleApiClientRequestCode = 0;
    boolean wasInBackground = false;

    private static In15minutesApp mInstance;
    private RequestQueue mRequestQueue;
    static SharedPreferences preferences;
    private Timer activityTransitionTimer;
    private TimerTask activityTransitionTimerTask;

    @Override
    public void onCreate()
    {
        super.onCreate();

        // Called only during app's fresh launch

        mInstance = this;
        mRequestQueue = Volley.newRequestQueue(this);
        preferences = getSharedPreferences(PREFS_NAME, 0);

        if (locationRequest == null)
        {
            locationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(5 * 1000)        // 5 seconds, in milliseconds
                    .setFastestInterval(2 * 1000); // 2 seconds, in milliseconds
        }

        if (googleApiClientRequestCode == 0)
        {
            Random r = new Random();
            int min = 1000;
            int max = 10000;

            googleApiClientRequestCode = r.nextInt((max - min) + 1) + min;
        }
    }

    //----------------------------------------------------------------------------------------//
    // Get instance singleton
    //----------------------------------------------------------------------------------------//

    public static synchronized In15minutesApp getInstance() { return mInstance; }

    //----------------------------------------------------------------------------------------//
    // Volley
    //----------------------------------------------------------------------------------------//

    public void addJsonRequestToQueue(JsonObjectRequest jsObjRequest)
    {
        jsObjRequest.setTag(TAG);
        mRequestQueue.add(jsObjRequest);
    }

    private void cancelPendingRequests()
    {
        Log.i(TAG, "cancelPendingRequests()");
        mRequestQueue.cancelAll(TAG);
    }

    //----------------------------------------------------------------------------------------//
    // Timers
    //----------------------------------------------------------------------------------------//

    public void startActivityTransitionTimer()
    {
        if (activityTransitionTimer != null) { return; }

        Log.i(TAG, "startActivityTransitionTimer()");

        activityTransitionTimer = new Timer();

        activityTransitionTimerTask = new TimerTask()
        {
            public void run()
            {
                Log.i(TAG, "Timer task executed");

                wasInBackground = true;
                cancelPendingRequests();

                activityTransitionTimer = null;
                activityTransitionTimerTask = null;
            }
        };

        long delay = 1500;
        activityTransitionTimer.schedule(activityTransitionTimerTask, delay);
    }

    public void stopActivityTransitionTimer()
    {
        if (activityTransitionTimer == null) { return; }

        Log.i(TAG, "stopActivityTransitionTimer()");

        activityTransitionTimer.cancel();
        activityTransitionTimer = null;
        activityTransitionTimerTask.cancel();
        activityTransitionTimerTask = null;
    }
}
